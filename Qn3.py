#Write a program that  takes an integer as input and  returns True if the input is power of two.


def is_p_of_two(n):
    return n>0 and (n&(n-1)) == 0

#input 
num= int(input("Enter an Integer"))


result=is_p_of_two(num)

#print results
if result:
    print(f"{num} is power of two.")
else:
    print(f"{num} is not power of two.")