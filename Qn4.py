#Write a program that accepts a string as input, capitalises the first letter of each word in the string, 
# and the returns the result string.

#function.
def cap_f_letter(Input_string):
    words=Input_string.split()
    cap_words=[word.capitalize() for word in words]
    res_string=''.join(cap_words)
    return res_string

#input from user.
Input_string=input("Enter a string:")

#variable declaration and function calling.
result=cap_f_letter(Input_string)

#Printing results.
print("Cap String:",result)

