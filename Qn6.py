#Write a program that counts the number of vowels in a sentence.

#function for peforming the task of counting.
def count_Vowels(sent):
    vowels="aeiouAEIOU"
    vowel_count=0
    
    for char in sent:
        if char in vowels:
            vowel_count+=1
    return vowel_count


#input from user.
sent=input("Enter a sentence:")

#declaring a variable for holding the result and calling the function.
vowels_count=count_Vowels(sent)

#Printing the result from the function.
print("Vowels in the sentnce:",vowels_count)