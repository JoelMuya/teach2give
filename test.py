word="Joel Muya"
print(word.split(' '))

start=word.startswith('J')
end=word.endswith('a')
print(start)
print(end)

mystr="Joel Muya is a good boy"


print('',mystr.upper())
print('',mystr.lower())
print('',mystr.title())
print('',mystr.capitalize())
print('',mystr.swapcase())

print(''.join(reversed(word)))

#using indexing operator
print(word[::-1])

str="*****Muya******"
print(str.strip('*'))
print(str.lstrip('*'))
print(str.rstrip('*'))

##replace string ie to be replaced, to replace...
str1="Hello, Muya"
print(str1.replace("Hello","Goodbye"))


int=1.0022143
print("format int is %1.3f" %int)

nam="Joel Muya \t He is a good boy"
n="is"
print(nam.index(n, 0, len(nam)))

print(nam.rjust(30,' '))