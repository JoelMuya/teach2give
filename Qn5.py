#Write a program that takes an integer as input and returns an integer with reversed digit ordering.

#funtion.
def rev_int(input_int):
    rev_str=str(input_int)[::-1]
    if input_int < 0:
        rev_str="-"+rev_str[:-1]
    return int(rev_str)
    
    
#taking input from user.
input_int=int(input("Enter an Integer: "))

#variable and function calling.
result=rev_int(input_int)

#printing result.
print("Integer with reversed digit ordering:",result)